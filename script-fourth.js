function fib(n,f0 = 0,f1 = 1){
    let result = 0;
    if(n === 0){
        return f0;
    } else if (n === 1 || n === -1){
        return f1;
    } else if(n >=2){
        for(let i = 2; i <=n; i++){
            result = f0 + f1;
            f0 = f1;
            f1 = result;
        }
    } else if(n<0){
        for(let i = -2;i >= n ; i--){   
            result = f0 - f1;
            f0 = f1;
            f1 = result;
        }
    }
    return result;
}

let n = +prompt('Enter your Number', '');

// n, f0, f1

alert(`Your value is: ${fib(n)}`);
 
// console.log(fib(n));



